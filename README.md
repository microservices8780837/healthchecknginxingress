# Sources:

1. https://kubernetes.io/docs/reference/kubectl/ - команды kubctl
2. https://ru.werf.io/guides/laravel/100_basic/38_kubernetes_basics.html - Основы Kubernetes
3. https://itsecforu.ru/2019/12/09/%E2%98%B8%EF%B8%8F-%D1%83%D1%87%D0%B5%D0%B1%D0%BD%D0%BE%D0%B5-%D0%BF%D0%BE%D1%81%D0%BE%D0%B1%D0%B8%D0%B5-%D0%BF%D0%BE-ingress-%D0%B4%D0%BB%D1%8F-%D0%BD%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D1%8E%D1%89%D0%B8/

# Домашнее задание

Основы работы с Kubernetes (Часть 2)

### Цель:

В этом ДЗ вы научитесь создавать минимальный сервис.

### Описание/Пошаговая инструкция выполнения домашнего задания:

* Шаг 1. Создать минимальный сервис, который\
  отвечает на порту 8000\
  имеет http-метод\
  `GET /health/`\
  RESPONSE: `{"status": "OK"}`\
* Шаг 2. Cобрать локально образ приложения в докер.\
  Запушить образ в dockerhub
* Шаг 3. Написать манифесты для деплоя в k8s для этого сервиса.\
  Манифесты должны описывать сущности: Deployment, Service, Ingress.\
  В Deployment могут быть указаны Liveness, Readiness пробы.\
  Количество реплик должно быть не меньше 2. Image контейнера должен быть указан с Dockerhub.\
  Хост в ингрессе должен быть `arch.homework`.\
  В итоге после применения манифестов GET запрос на `http://arch.homework/health` должен отдавать `{“status”: “OK”}`.\
* Шаг 4. На выходе предоставить

0) ссылку на github c манифестами. Манифесты должны лежать в одной директории, так чтобы можно было их все применить
   одной командой kubectl apply -f.\
   url, по которому можно будет получить ответ от сервиса (либо тест в postmanе).\

Задание со звездой (+5 баллов):*
В Ingress-е должно быть правило, которое форвардит все запросы с /otusapp/{student name}/* на сервис с rewrite-ом пути.
Где {student name} - это имя студента.\
Например: curl arch.homework/otusapp/aeugene/health -> рерайт пути на arch.homework/health\
Рекомендации по форме сдачи дз:\

использовать nginx ingress контроллер, установленный через хелм, а не встроенный в миникубик:\
kubectl create namespace m && helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx/ && helm repo
update && helm install nginx ingress-nginx/ingress-nginx --namespace m -f nginx-ingress.yaml (файл приложен к занятию)\
https://kubernetes.github.io/ingress-nginx/user-guide/basic-usage/\
необходимо в новых версиях nginx добавлять класс ингресса\
ingressClassName: nginx\
прикладывать к 2 дз урл для проверки: curl http://arch.homework/health или как указано в дз со *.\
К 3 дз и далее дз прикладывать коллекцию postman и проверять ее работу через newman run имя_коллекции\
прикладывать кроме команд разворачивания приложения, команду удаления)\
прописать у себя в /etc/hosts хост arch.homework с адресом своего миникубика (minikube ip), чтобы обращение было по
имени хоста в запросах, а не айпи\
у коллег на MacOS возможно не получиться настроить доступ по minikube ip, тогда прокидываем туннель minikube tunnel и
используем айпи адрес 127.0.0.1\
Обратите внимание, что при сборке на m1 при запуске вашего контейнера на стандартных платформах будет ошибка такого
вида:\
standard_init_linux.go:228: exec user process caued: exec format error\
Для сборки рекомендую указать тип платформы linux/amd64:\
docker build --platform linux/amd64 -t tag .\
Более подробно можно прочитать в
статье: https://programmerah.com/how-to-solve-docker-run-error-standard_init_linux-go219-exec-user-process-caused-exec-format-error-39221/

# Шаги реализации

1. Образ dockerhub homework1

2. Включить kubernetes в DockerDesktop
   ![img.png](img.png)

3. `kubectl get node` - проверить доступные ноды

```text
NAME             STATUS   ROLES           AGE    VERSION
docker-desktop   Ready    control-plane   5d1h   v1.24.1
```

4. `kubectl create namespace m && helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx/ && helm repo update && helm install nginx ingress-nginx/ingress-nginx --namespace m -f nginx_ingress.yaml`

```text
namespace/m created
"ingress-nginx" has been added to your repositories
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "ingress-nginx" chart repository
Update Complete. ⎈Happy Helming!⎈
NAME: nginx
LAST DEPLOYED: Fri Sep 29 00:29:23 2023
NAMESPACE: m
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The ingress-nginx controller has been installed.
Get the application URL by running these commands:
  export HTTP_NODE_PORT=$(kubectl --namespace m get services -o jsonpath="{.spec.ports[0].nodePort}" nginx-ingress-nginx-controller)
  export HTTPS_NODE_PORT=$(kubectl --namespace m get services -o jsonpath="{.spec.ports[1].nodePort}" nginx-ingress-nginx-controller)
  export NODE_IP=$(kubectl --namespace m get nodes -o jsonpath="{.items[0].status.addresses[1].address}")

  echo "Visit http://$NODE_IP:$HTTP_NODE_PORT to access your application via HTTP."
  echo "Visit https://$NODE_IP:$HTTPS_NODE_PORT to access your application via HTTPS."

An example Ingress that makes use of the controller:
  apiVersion: networking.k8s.io/v1
  kind: Ingress
  metadata:
    name: example
    namespace: foo
  spec:
    ingressClassName: nginx
    rules:
      - host: www.example.com
        http:
          paths:
            - pathType: Prefix
              backend:
                service:
                  name: exampleService
                  port:
                    number: 80
              path: /
    # This section is only required if TLS is to be enabled for the Ingress
    tls:
      - hosts:
        - www.example.com
        secretName: example-tls

If TLS is enabled for the Ingress, a Secret containing the certificate and key must also be provided:

  apiVersion: v1
  kind: Secret
  metadata:
    name: example-tls
    namespace: foo
  data:
    tls.crt: <base64 encoded cert>
    tls.key: <base64 encoded key>
  type: kubernetes.io/tls
```

5. `helm list -d`
6. `kubectl get ns`

```text
   NAME              STATUS   AGE
   default           Active   2d2h
   kube-node-lease   Active   2d2h
   kube-public       Active   2d2h
   kube-system       Active   2d2h
   m                 Active   3m58s
```

7. `kubectl get svc -n kube-system` - To check your DNS settings, you can use the following command
8. `kubectl get pod -A` - Check pods of all namespaces
9. `kubectl apply -f .`
10. `env` - local variables
11. `kubectl port-forward healthcheck-dp-6797c5584b-28gm8 11111:8000` - forward port

```text
http://localhost:11111/health/
{
  "status": "OK"
}
```

12. Настроить манифесты на LoadBalancer
13. `kubectl apply -f .`
14. `C:\Windows\System32\drivers\etc` добавить новый хост к `127.0.0.1 kubernetes.docker.internal arch.homework`
15. `kubectl get svc`

```text
     NAME              TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
     healthcheck-svc   LoadBalancer   10.103.250.115   localhost     80:30707/TCP   160m
     kubernetes        ClusterIP      10.96.0.1        <none>        443/TCP        19d
```

16. `kubectl get ing`

```text
    NAME                  CLASS   HOSTS           ADDRESS          PORTS   AGE
    healthcheck-ingress   nginx   arch.homework   10.101.220.134   80      163m
```

17. `kubectl get deployments`

```text
    NAME             READY   UP-TO-DATE   AVAILABLE   AGE
    healthcheck-dp   2/2     2            2           164m
```

18. `kubectl get pods`

```text
    NAME                              READY   STATUS    RESTARTS   AGE
    healthcheck-dp-7468999589-7bm9b   1/1     Running   0          164m
    healthcheck-dp-7468999589-b5bck   1/1     Running   0          164m
```

19. `kubectl get all`

```text
    NAME                                  READY   STATUS    RESTARTS   AGE
    pod/healthcheck-dp-7468999589-7bm9b   1/1     Running   0          155m
    pod/healthcheck-dp-7468999589-b5bck   1/1     Running   0          155m
    
    NAME                      TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
    service/healthcheck-svc   LoadBalancer   10.103.250.115   localhost     80:30707/TCP   155m
    service/kubernetes        ClusterIP      10.96.0.1        <none>        443/TCP        19d
    
    NAME                             READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/healthcheck-dp   2/2     2            2           155m
    
    NAME                                        DESIRED   CURRENT   READY   AGE
    replicaset.apps/healthcheck-dp-7468999589   2         2         2       155m

```

20. `kubectl get services -n m` проверка контроллера

```text
    NAME                                       TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
    nginx-ingress-nginx-controller             NodePort    10.101.220.134   <none>        80:30998/TCP,443:30549/TCP   16d
    nginx-ingress-nginx-controller-admission   ClusterIP   10.111.196.193   <none>        443/TCP                      16d

```

21. Удаление сущностей

kubectl delete ingress healthcheck-ingress
| kubectl delete deployments healthcheck-dp 
| kubectl delete services healthcheck-svc

---

Результат запуска программы:

![img_1.png](img_1.png)

---

С помощью Deployment’а мы можем развернуть наше stateless-приложение, но если пользователям или другим приложениям
потребуется связываться с этим приложением изнутри или снаружи кластера, то в этом нам помогут два других ресурса:
Ingress и Service. Ключевые вещи, которые должны помочь понять объект ingress.

1. Вы должны создать правила Ingress в том же пространстве имен, в котором развернуты службы. Вы не можете направлять
   трафик на службу в другом пространстве имен, где у вас нет Ingress объекта.
2. Ingress объект требует ingress controller для маршрутизации трафика.
3. Внешний трафик не будет попадать на Ingress API, вместо этого он будет попадать на службу ingress controller

Доп. команды

1. `kubectl create ns app` - Create ns app
2. `kubectl set-context --current --namespace=app` - to avoid specifying(pointing) a location every time
3. `winpty kubectl exec -it healthcheck-dp-6797c5584b-28gm8 -- sh` - start alpine
4. `kubectl logs healthcheck-dp-6797c5584b-28gm8`
5. `kubectl logs kuber-dp-7df8b94f96-cmrms --all-containers`
6. `kubectl get pod --show-labels`
7. `kubectl get rs --show-labels` - check replica sets
8. `kubectl rollout history deployment healthcheck-dp` - rollout history
9. `kubectl apply -f healthcheck_deployment.yaml --record` - откатиться, поставить метку
10. `kubectl annotate deployment healthcheck-dp kubernates.io/change-cause="new version"` - откатиться, поставить метку
11. `kubectl rollout undo deployment healthcheck-dp` - rollout history
12. `kubectl describe pod kuber-dp-597cbfcc87-7tvmc`
13. `kubectl delete pods kuber-dp-597cbfcc87-7tvmc kuber-dp-597cbfcc87-dfstz`
14. `kubectl delete deployments kuber-dp`
15. `kubectl delete service kuber-dp`
16. `kubectl delete namespace m` - удалить namespace m
17. `kubectl get ns` - получить все namespaces
18. `kubectl get all -n m` - получить данные pods в namespace m